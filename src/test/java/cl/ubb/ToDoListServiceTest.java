package cl.ubb;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import cl.ubb.dao.ToDoDao;
import cl.ubb.model.ToDo;
import cl.ubb.service.ToDoService;
import junit.framework.Assert;


@RunWith(MockitoJUnitRunner.class)
public class ToDoListServiceTest {

	@Mock
	private ToDoDao todolistDao;
	
	@InjectMocks
	private ToDoService todolistService;
	
	@Test
	public void obtenerToDoPorId(){
		
		//arrange
		ToDo toDo = new ToDo();
		//act
		when(todolistDao.findOne(anyLong())).thenReturn(toDo); 
		ToDo toDoCreado = todolistService.retornarToDo(anyLong());
		//assert
		Assert.assertNotNull(toDoCreado);
		Assert.assertEquals(toDo, toDoCreado);
		
	}
	
	@Test
	public void obtenerToDoPorCategoria(){
		
		//arrange
		ToDo toDo = new ToDo();
		//act
		when(todolistDao.findByCategoria(anyString())).thenReturn(toDo); 
		ToDo toDoCreado = todolistService.retornarToDo(anyString());
		//assert
		Assert.assertNotNull(toDoCreado);
		Assert.assertEquals(toDo, toDoCreado);
		
	}
	
	@Test
	public void obtenerToDoPorEstado(){
		
		//arrange
		ToDo toDo = new ToDo();
		//act
		when(todolistDao.findByEstado(anyBoolean())).thenReturn(toDo); 
		ToDo toDoCreado = todolistService.retornarToDo(anyBoolean());
		//assert
		Assert.assertNotNull(toDoCreado);
		Assert.assertEquals(toDo, toDoCreado);
		
	}
	
	@Test
	public void obtenertodosLosToDoEnUnaLista(){
		
		//arrange
		List<ToDo> toDoList = new ArrayList<ToDo>();
		//act
		when(todolistDao.findAll()).thenReturn(toDoList); 
		List<ToDo> toDoCreado = todolistService.retornarAllToDo();
		//assert
		Assert.assertNotNull(toDoCreado);
		Assert.assertEquals(toDoList, toDoCreado);
		
	}
	
	@Test
	public void guardarToDo(){
		
		//arrange
		ToDo toDo = new ToDo();
		//act
		when(todolistDao.save(toDo)).thenReturn(toDo); 
		todolistService.saveToDo(toDo);
		//assert
		
	}
	
	@Test
	public void eliminarToDo(){
		
		//arrange
		ToDo toDo = new ToDo();
		//act
		todolistService.deleteToDo(toDo);
		//assert
		
	}

	
}