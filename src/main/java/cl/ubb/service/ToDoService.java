package cl.ubb.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import cl.ubb.dao.ToDoDao;
import cl.ubb.model.ToDo;

public class ToDoService {
	
	private final ToDoDao toDoDao;
	
	@Autowired
	public ToDoService(ToDoDao toDoDao){
		this.toDoDao = toDoDao;
	}
	
	public ToDo retornarToDo(Long i){
		
		ToDo toDo = new ToDo();
		
		toDo = toDoDao.findOne(i);
		
		return toDo;
	}
	
	public ToDo retornarToDo(String categoria){
		
		ToDo toDo = new ToDo();
		
		toDo = toDoDao.findByCategoria(categoria);
		
		return toDo;
	}
	
	public ToDo retornarToDo(Boolean estado){
		
		ToDo toDo = new ToDo();
		
		toDo = toDoDao.findByEstado(estado);
		
		return toDo;
	}
	
	public List<ToDo> retornarAllToDo(){
		
		List<ToDo> toDoList = new ArrayList<ToDo>();
		
		toDoList = (List<ToDo>) toDoDao.findAll();
		
		return toDoList;
	}
	
	public void saveToDo(ToDo toDo){
		
		toDoDao.save(toDo);
		
	}
	
	public void deleteToDo(ToDo toDo){
		
		toDoDao.delete(toDo);
		
	}
}
